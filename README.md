# Bachelorette Draft

## Installation

Ensure Python 2.7 and pip are installed. Then run:

```
~/bachelor/app/ $ pip install -r requirements.txt
```

to install all dependencies. Next, ensure you have MongoDB 3.0 installed. In a separate terminal, make a `./data/` directory, and run

```
~/bachelor/ $ mongod --dbpath data
```

Next, you'll want to run the `~/bachelor/cron/update_contestants.py` to scrape ABC's Bachelorette Cast page and push to mongo (under bachelor.contestants). It will also add it to a crontab owned by root.

```
~/bachelor/cron $ pip install -r requirements.txt
~/bachelor/cron $ ./update_contestants.py
```

## Running

To start the server, run

```
~/bachelor/app $ ./bachelorette.py
```

## Docker-Compose

Alternatively, rather than running via the above steps, ensure you have Docker and Docker-Compose installed and simply run

```
~/bachelor/ $ docker-compose up
```

This will build 3 containers:
- A MongoDB container
- The Flask Web Application
- A cronjob process that will seed the database weekly on Sundays

## Rules and Scoring System

Based on [this scoring system](http://www.waroftherosesfantasyleague.com/scoring-systems.html):
- 6 points
    - Contestants receives a rose signifying he will be moving on in the competition.
- 6 points
    - Contestant leaves outside the spirit of the rose ceremony (this includes the cocktail party prior to the start of the rose ceremony).
- 3 points
    - Contestant physically assaults or is assaulted by another contestant (ie. slap, shove, punch, etc. with malicious intent).  Limit 1 per commercial break.
- 3 points
    - Contestant cries for any reason.
- 2 points
    - Contestant is "bleeped out" or physically censored (middle finger, pixelated private parts).  Limit 1 per commercial break.
- 2 points
    - Contestant says the word/phrase "journey" or "right reasons" at which point, players must yell out the phrase and take a drink of their beverage!
- 1 point
    - Contestant kisses anyone on the lips by any person for any reason.  Limit 1 per embrace.

## TODO

- [x] Auth
    - [x] Get login/logout working
    - [x] Add user greeting
    - [x] Add ability to signup
    - [x] Link to signup page on homepage
- [x] Web scraping ABC's cast page
    - [x] Seed mongo database with show contestants
    - [x] Create cron to run weekly
    - [x] Determine who has been eliminated
    - [x] Dockerize
- [ ] Styling
    - [x] Fix login styling
    - [x] Fix signup styling
    - [x] Fix alert styling
    - [ ] 
- [ ] Draft functionality
    - [ ] Add ability to select contestants
    - [ ] Add API endpoint for pushing selections
    - [ ] Push selections to DB
    - [ ] Add key-value pairs related to the draft (e.g., "kiss", "curse", "drunk", etc.)
    - [ ] Add key-value pairs to DB
    - [ ] Calculate score of each individual player
- [ ] Leaderboard
- [x] Docker-Compose everything
