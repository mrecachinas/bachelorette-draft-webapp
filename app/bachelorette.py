#!/usr/bin/env python
import os
import logging
import pymongo
import bcrypt
from flask import (
        Flask,
        request,
        session,
        redirect,
        url_for,
        abort,
        g,
        flash,
        render_template
)

DATABASE_NAME = 'bachelor'
DEBUG = True
SECRET_KEY = 'foobar'

app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def home():
    db_name = app.config["DATABASE_NAME"]
    contestants = list(g.db[db_name]["contestants"].find())[0]["contestants"]
    return render_template('index.html', contestants=contestants)

@app.route('/create_user', methods=['GET', 'POST'])
def create_user():
    if request.method == 'POST':
        user_name = request.form["user_name"]
        user_email = request.form["user_email"]
        password = request.form["password"]
        user = create_user(user_name, user_email, password)
        session['logged_in'] = True
        session['user_name'] = user['user_name']
        return redirect(url_for('home'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    print "LOGIN"
    if request.method == 'POST':
        print "YO"
        user_name = request.form['user_name']
        password = request.form['password']
        user = auth_user(user_name, password)
        if not user:
            error = "Invalid username or password"
        else:
            session['logged_in'] = True
            session['user_name'] = user['user_name']
            flash('You were logged in')
            return redirect(url_for('home'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))

@app.route('/signup')
def signup():
    return render_template('signup.html')

@app.route('/update_scores', methods=['POST'])
def update_scores():
    return

@app.route('/scores')
def scores():
    db_name = app.config['DATABASE_NAME']
    scores = g.db[db_name]['']
    return render_templates('scores.html', scores=scores)

@app.before_request
def before_req():
    db_name = app.config['DATABASE_NAME']
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

def connect_db():
    db_ip = os.environ['DB_PORT_27017_TCP_ADDR']
    return pymongo.MongoClient(db_ip, 27017)

def create_user(user_name, user_email, password):
    db_name = app.config['DATABASE_NAME']
    existing_user = auth_user(user_name, password)
    if existing_user:
        return existing_user
    password = password.encode('utf-8')
    salt = bcrypt.gensalt()
    user = {
        'user_name': user_name,
        'user_email': user_email,
        'password': bcrypt.hashpw(password, salt)
    }
    g.db[db_name]['users'].insert(user)
    return user

def auth_user(user_name, password):
    db_name = app.config['DATABASE_NAME']
    user = g.db[db_name]['users'].find_one({"user_name": user_name})
    if not user:
        return None
    stored_hash = user['password'].encode('utf-8')
    if bcrypt.hashpw(password.encode('utf-8'), stored_hash) == stored_hash:
        return user
    else:
        return None

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
