#!/usr/bin/env python
import pymongo
import logging
from bs4 import BeautifulSoup
import urllib2
import datetime
import time
import os
from PIL import Image, ImageStat
import cStringIO

def determine_if_eliminated(contestant_img_link):
    f = cStringIO.StringIO(urllib2.urlopen(contestant_img_link).read())
    MONOCHROMATIC_MAX_VARIANCE = 0.005
    COLOR = 1000
    MAYBE_COLOR = 100

    v = ImageStat.Stat(Image.open(f)).var
    is_monochromatic = reduce(lambda x, y: x and y < MONOCHROMATIC_MAX_VARIANCE, v, True)
    if is_monochromatic:
        return True
    else:
        if len(v) == 3:
            maxmin = abs(max(v) - min(v))
            if maxmin > COLOR:
                return False
            elif maxmin > MAYBE_COLOR:
                return False
            else:
                return True
        elif len(v)==1:
            return True
        else:
            return False

def update(contestants, mongo_ip=os.environ['DB_PORT_27017_TCP_ADDR'], db_name="bachelor"):
    db = pymongo.MongoClient(mongo_ip, 27017)[db_name]
    db["contestants"].insert({
        "datetime": datetime.datetime.now(),
        "contestants": contestants
    })

def get_contestants():
    html = urllib2.urlopen('http://abc.go.com/shows/the-bachelorette/cast').read()
    soup = BeautifulSoup(html, "lxml")
    contestants = soup.find_all("li", class_="tile-content-overlay")
    contestants_dict = {}
    for contestant in contestants:
        contestant_name = contestant.find("span", class_="text3 title truncate")
        if contestant_name:
            name = contestant_name.text.strip()
            if name.endswith("."):
                name = name[:-1]
        else:
            continue
        contestant_img  = contestant.find("img")
        if contestant_img:
            img = contestant_img["src"].strip()
        else:
            img = None
        contestant_role = contestant.find("span", class_="text4 subheader host-type extra-light")
        if contestant_role:
            role = contestant_role.text.strip()
        else:
            role = None
        contestant_page = contestant.find("a")
        if contestant_page:
            page = contestant_page["href"].strip()
        else:
            page = None
        contestants_dict[name] = {
                "img": img,
                "page": "https://abc.go.com/" + page,
                "role": role,
                "eliminated": determine_if_eliminated(img)
        }
    return contestants_dict

if __name__ == '__main__':
    sunday = 6

    # update when it's run to initially populate the DB
    contestants = get_contestants()
    update(contestants)

    # enter "cron-mode"
    while True:
        if not (datetime.datetime.today().weekday() == sunday and \
           datetime.time(0, 0) <= datetime.datetime.now().time() <= datetime.time(0, 1)):
            today = datetime.date.today()
            days_til_sunday = datetime.timedelta((sunday - today.weekday()) % 7)
            time.sleep(days_til_sunday.total_seconds())
        else:
            contestants = get_contestants()
            update(contestants)
